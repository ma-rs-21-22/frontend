# frontend ma-ui
Prototypische Benutzer*innenoberfläche für die Masterarbeit "Visualisierung von Open Data - Analyse und Konzeptentwicklung zur Einbindung von Visualisierung innerhalb eines Open Data Portals am Beispiel von Data.Europa.eu".

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
